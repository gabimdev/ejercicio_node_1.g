const express = require('express');
const port = 3000;
const server = express();

const router = express.Router();

const movies = require('./movies.js');

const moviesNames = [];
movies.forEach(movie => {
    moviesNames.push(movie.name);
});

router.get('/', (req, res) => {
    res.send('home');
});


router.get('/movies', (req, res) => {
    const moviesArray = moviesNames;
    const { name } = req.query;
    if (!name) {
        res.send(moviesArray);
    } else {
        const hasMovie = moviesArray.some((movie) => movie.toLowerCase() === name.toLowerCase());
        if (hasMovie) {
            return res.send(`We have a movie with the name ${name}`);
        } else {
            return res.send('We could not find the movie you are looking for!');
        }
    }
});


router.get('/movies/:id', (req, res) => {
    const { id } = req.params;
    const result = movies.filter(movie => movie.id === id)
    if (result) {
        return res.send(result.name);
    } else {
        return res.send('We could not find the movie you are looking for!');
    }
});

router.get('/movieso', (req, res) => {
    res.send(movies);
});

router.get('/movies', (req, res) => {
    res.send(moviesNames);
});




server.use('/', router);





server.listen(port, () => {
    console.log(`Server runnig in http://localhost:${port}`);
});